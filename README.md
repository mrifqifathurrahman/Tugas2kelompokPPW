# Tugas 2 - Kelas B - Kelompok 13

Web Design & Programming (Perancangan & Pemrograman Web) @
Faculty of Computer Science Universitas Indonesia, Odd Semester 2017/2018

Anggota Kelompok:
	- Adib Yusril Wafi
	- Intan Wulandari
	- Mohammad Ahnaf Zain Ilham
	- Muhammad Rifqi Fathurrahman

Staus Pipeline: [![pipeline status](https://gitlab.com/mrifqifathurrahman/Tugas2kelompokPPW/badges/master/pipeline.svg)](https://gitlab.com/mrifqifathurrahman/Tugas2kelompokPPW/commits/master)

	

Status Coverage: [![coverage report](https://gitlab.com/mrifqifathurrahman/Tugas2kelompokPPW/badges/master/coverage.svg)](https://gitlab.com/mrifqifathurrahman/Tugas2kelompokPPW/commits/master)


Link herokuapp:
https://tugas2-13.herokuapp.com/