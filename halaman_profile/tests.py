from django.test import TestCase
from django.test import Client
from django.urls import resolve

import environ
from .views import index
from django.http import HttpRequest
from datetime import date
import unittest




root = environ.Path(__file__) - 3  # three folder back (/a/b/c/ - 3 = /)
env = environ.Env(DEBUG=(bool, False),)
environ.Env.read_env('.env')

# Create your tests here.
class Tugas2UnitTest_Profile(TestCase):
    def setUp(self):
        self.username = env("SSO_USERNAME")
        self.password = env("SSO_PASSWORD")

    def test_direct_access_to_status_url(self):
        # not logged in, redirect to login page
        response = self.client.get('/fitur2/')
        self.assertEqual(response.status_code, 302)

         # not logged in, redirect to login page
        response = self.client.get('/fitur2/')
        self.assertEqual(response.status_code, 302)

        # logged in, render profile template
        response = self.client.post(
            '/fitur1/custom_auth/login/', {'username': self.username, 'password': self.password})
        self.assertEqual(response.status_code, 302)
        
        response = self.client.get('/fitur2/')
        self.assertEqual(response.status_code, 200)

        response = self.client.get('/fitur2/')
        self.assertEqual(response.status_code, 200)	

        response = self.client.get('/fitur2/edit/')
        self.assertEqual(response.status_code, 200)

    def Profile_is_exist(self):
        response = Client().get('/fitur2/')
        self.assertEqual(response.status_code,200)

    def Profile_is_using_index_func(self):
        found = resolve('/fitur2/')
        self.assertEqual(found.func, index)

