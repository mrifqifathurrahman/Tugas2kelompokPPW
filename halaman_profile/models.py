from __future__ import unicode_literals
from django.db import models


# Create your models here.

class User(models.Model):
    kode_identitas = models.CharField('Kode Identitas', max_length=20, primary_key=True, default='Kosong')
    name = models.CharField('Nama', max_length=225)
    email = models.EmailField('Email', default='Kosong')
    profileUrl = models.URLField('Profile LinkedIn', default='Kosong')
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

class Expertise(models.Model):
    kode_identitas = models.CharField('Kode Identitas', max_length=27)
    expert = models.CharField('Keahlian', max_length=27, default='Kosong')
    kejagoan = models.CharField('Kejagoan', max_length=27, default='Kosong')
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)
