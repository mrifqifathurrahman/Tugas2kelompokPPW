from __future__ import unicode_literals
from django.views.decorators.csrf import csrf_exempt

from django.shortcuts import render
from django.http import HttpResponseRedirect
from django.urls import reverse
from django.contrib import messages
from .models import User, Expertise


response = {}

def index(request):
    if 'user_login' in request.session:
        response['name'] = 'Tugas 2 PPW'
        html = 'halaman_profile/halaman_profile.html'
        response['login'] = True
        response['kode_identitas'] = request.session['kode_identitas']
        exist = User.objects.filter(kode_identitas=1).exists()
        response['jumlahKosong'] = set_data_user(request, exist)
        response['nama_identitas'] = request.session['user_login']


        return render(request, html, response)

    else:
        response['connected'] = False #digunakan sebagai penanda kapan header menampilkan tombol logout
        response['name'] = 'Tugas 2 PPW'
        response['login'] = False
        html = 'halaman_login_status/halaman_status.html'
        return HttpResponseRedirect(request, html, response)

def edit_profile(request):
    if 'user_login' in request.session:
        response['name'] = 'Tugas 2 PPW'
        html = 'halaman_profile/edit_profile.html'
        return render(request, html, response)
    else:
        response['connected'] = False #digunakan sebagai penanda kapan header menampilkan tombol logout
        response['name'] = 'Tugas 2 PPW'
        response['login'] = False
        html = 'halaman_login_status/halaman_status.html'
        return render(request, html, response)

@csrf_exempt
def save_database(request):
    if(request.method == 'POST'):
        name = request.POST['firstName'] + ' ' + request.POST['lastName']
        email = request.POST['email']
        profileUrl = request.POST['profileUrl']
        kode_identitas = 1
        user = User(name=name, email=email, profileUrl=profileUrl, kode_identitas=kode_identitas)
        user.save()

def set_data_user(request, exist):
	counterKosong = 0
	if exist:
		user = User.objects.get(kode_identitas=1)
		response['userName'] = user.name
		response['userEmail'] = user.email
		response['userProfileLinkedIn'] = user.profileUrl
	else:
		counterKosong = 3
		response['userName'] = 'kosong'
		response['userEmail'] = 'kosong'
		response['userProfileLinkedIn'] = 'kosong'
	expertiseExist = Expertise.objects.filter(kode_identitas=1).exists()
	if not expertiseExist:
		counterKosong += 1
		response['userKeahlian'] = 'kosong'
	return counterKosong


