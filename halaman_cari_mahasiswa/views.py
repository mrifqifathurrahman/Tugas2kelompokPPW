from django.shortcuts import render
from django.http import HttpResponseRedirect
from django.urls import reverse
from halaman_profile.models import *

# Create your views here.
response = {}

def index(request):
    if 'user_login' in request.session:
    	set_dummy_data()
    	set_data = True
    	response['nama_identitas'] = request.session['user_login']
    	response['login'] = True

    	response['pengguna'] = User.objects.all()
    	response['keahlian'] = Expertise.objects.all()
    	html = "halaman_cari_mahasiswa/cari_mahasiswa.html"
    	return render(request, html, response)

    else:
    	return HttpResponseRedirect(reverse('fitur1:index'))

def set_dummy_data():
	if User.objects.all().count() != 0:
		return

	pengguna = User(name="Teman A", kode_identitas="1234567890")
	pengguna.save()

	pengguna = User(name="Teman B", kode_identitas="0123456789")
	pengguna.save()

	pengguna = User(name="Teman C", kode_identitas="9012345678")
	pengguna.save()

	pengguna = User(name="Teman D", kode_identitas="8901234567")
	pengguna.save()
	keahlian = Expertise(expert="Java", kejagoan="1", kode_identitas=pengguna.kode_identitas)
	keahlian.save()

	pengguna = User(name="Teman E", kode_identitas="7890123456")
	pengguna.save()
	keahlian = Expertise(expert="Java", kejagoan="2", kode_identitas=pengguna.kode_identitas)
	keahlian.save()

	pengguna = User(name="Teman F", kode_identitas="6789012345")
	pengguna.save()
	keahlian = Expertise(expert="Java", kejagoan="3", kode_identitas=pengguna.kode_identitas)
	keahlian.save()