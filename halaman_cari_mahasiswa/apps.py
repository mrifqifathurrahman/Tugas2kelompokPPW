from django.apps import AppConfig


class HalamanCariMahasiswaConfig(AppConfig):
    name = 'halaman_cari_mahasiswa'
