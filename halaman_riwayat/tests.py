from django.test import TestCase
from django.http import HttpRequest
from django.test import Client
from django.urls import resolve
from .views import index, get_riwayat
import requests

# Create your tests here.
class riwayatTest(TestCase):
    def test_halaman_riwayat_is_exist(self):
        response = Client().get('/riwayat/')
        self.assertEqual(response.status_code, 200)

    def test_halaman_riwayat_using_index_func(self):
        found = resolve('/riwayat/')
        self.assertEqual(found.func, index)