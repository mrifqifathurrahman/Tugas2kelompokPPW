from __future__ import unicode_literals
from django.shortcuts import render
from django.http import HttpResponseRedirect
from django.urls import reverse
from django.contrib import messages
from django.utils import timezone

from .forms import Status_Form
from .models import Status
# Create your views here.

response = {}

def dummy(request):
	if 'user_login' in request.session:
		return HttpResponseRedirect(reverse('fitur1:halaman_status'))
	html = 'halaman_login_status/session/dummy.html'
	return render(request, html, response)

def index(request):
	print("#==> masuk index")
	if 'user_login' in request.session:
		return HttpResponseRedirect(reverse('fitur1:halaman_status'))
	else:
		html = 'halaman_login_status/session/login.html'
		return render(request, html, response)

def halaman_status(request):
	print ("#==> halaman_status")

	if 'user_login' not in request.session.keys():
		return HttpResponseRedirect(reverse('fitur1:index'))

	set_data_for_session(response, request)
		
	html = 'halaman_login_status/layout/base.html'
	return render(request, html, response)		

def set_data_for_session(res, request):
	response['author'] = request.session['user_login']
	response['access_token'] = request.session['access_token']
	response['kode_identitas'] = request.session['kode_identitas']
	response['status'] = Status.objects.all()
	response['jumlah_status'] = Status.objects.all().count()
	response['status_form'] = Status_Form
	response['latest_post'] = Status.objects.last()

def add_status(request):
	form = Status_Form(request.POST or None)
	if (request.method == 'POST' and form.is_valid()):
		response['status'] = request.POST['status']
		status = Status(status=response['status'])
		status.save()
		return HttpResponseRedirect('/fitur1/halaman_status')
	else:
		return HttpResponseRedirect('/fitur1/halaman_status')	