from django import forms

class Status_Form(forms.Form):
	error_message = {
		'required': 'Have nothing to post?',
	}
	status_attrs = {
		'type': 'text',
		'cols': 147,
		'rows': 4,
		'class': 'status-form-textarea',
		'placeholder': "Write your day here!"
	}

	status = forms.CharField(label='', required=True, widget=forms.Textarea(attrs=status_attrs))