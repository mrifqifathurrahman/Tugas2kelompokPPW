from django.test import TestCase
from django.test import Client
from django.urls import resolve
from .views import index, add_status, dummy, halaman_status
from .models import Status
from .forms import Status_Form

from .custom_auth import auth_login, auth_logout
from .csui_helper import get_access_token

import environ

root = environ.Path(__file__) - 3  # three folder back (/a/b/c/ - 3 = /)
env = environ.Env(DEBUG=(bool, False),)
environ.Env.read_env('.env')

# Create your tests here.
class Tugas2UnitTest(TestCase):
    def setUp(self):
        self.username = env("SSO_USERNAME")
        self.password = env("SSO_PASSWORD")

    def test_fitur1_using_dummy_func(self):
        found = resolve('/fitur1/')
        self.assertEqual(found.func, dummy)

    def test_model_can_create_new_status(self):
        new_status = Status.objects.create(status='Mengerjakan Tugas 2 PPW SKS nih')

        counting_all_available_status = Status.objects.all().count()
        self.assertEqual(counting_all_available_status, 1)

    def test_status_form_validation_for_blank_items(self):
        form = Status_Form(data={'status': ''})
        self.assertFalse(form.is_valid())
        self.assertEqual(
        	form.errors['status'],
        	["This field is required."]
        )	

    def test_login_failed(self):
        response = self.client.post(
            '/fitur1/custom_auth/login/', {'username': "siapa", 'password': "saya"})
        html_response = self.client.get('/fitur1/').content.decode('utf-8')

    def test_fitur1_page_when_user_is_logged_in_or_not(self):
        # not logged in, render login template
        response = self.client.get('/fitur1/login/')
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed('halaman_login_status/session/login.html')

        # logged in, redirect to profile page
        response = self.client.post(
            '/fitur1/custom_auth/login/', {'username': self.username, 'password': self.password})
        self.assertEqual(response.status_code, 302)
        response = self.client.get('/fitur1/')
        self.assertEqual(response.status_code, 302)
        response = self.client.get('/fitur1/login/')
        self.assertEqual(response.status_code, 302)
        self.assertTemplateUsed('halaman_login_status/session/halaman_status.html')

    def test_status_post_success_and_render_the_result(self):
        test= 'Hidden'
        response_post = Client().post('/fitur1/add_status', {'status': test})
        self.assertEqual(response_post.status_code, 302)

    def test_update_status_post_error_and_render_the_result(self):
        test= 'Hidden'
        response_post = Client().post('/fitur1/add_status', {'status': ''})
        self.assertEqual(response_post.status_code, 302)

        response = Client().get('/fitur1/')
        html_response = response.content.decode('utf8')
        self.assertNotIn(test, html_response)

    def test_direct_access_to_status_url(self):
        # not logged in, redirect to login page
        response = self.client.get('/fitur1/halaman_status/')
        self.assertEqual(response.status_code, 302)

        # logged in, render profile template
        response = self.client.post(
            '/fitur1/custom_auth/login/', {'username': self.username, 'password': self.password})
        self.assertEqual(response.status_code, 302)
        response = self.client.get('/fitur1/halaman_status/')
        self.assertEqual(response.status_code, 200)	

    def test_logout(self):
        response = self.client.post(
            '/fitur1/custom_auth/login/', {'username': self.username, 'password': self.password})
        self.assertEqual(response.status_code, 302)
        response = self.client.post('/fitur1/custom_auth/logout/')
        html_response = self.client.get('/fitur1/').content.decode('utf-8')
        self.assertEqual(response.status_code, 302)

