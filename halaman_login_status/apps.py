from django.apps import AppConfig


class HalamanLoginStatusConfig(AppConfig):
    name = 'halaman_login_status'
