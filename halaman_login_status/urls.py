from django.conf.urls import url
from .views import index, add_status, dummy, halaman_status
from .custom_auth import auth_login, auth_logout


urlpatterns = [
	url(r'^$', dummy, name='dummy'),
	url(r'^login', index, name='index'),
	url(r'^halaman_status', halaman_status, name='halaman_status'),
	url(r'^add_status', add_status, name='add_status'),

	# custom auth
    url(r'^custom_auth/login/$', auth_login, name='auth_login'),
    url(r'^custom_auth/logout/$', auth_logout, name='auth_logout'),
]