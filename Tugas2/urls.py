"""Tugas2 URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.11/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf.urls import url, include
from django.contrib import admin
import halaman_login_status.urls as halaman_login_status
import halaman_profile.urls as halaman_profile
import halaman_riwayat.urls as halaman_riwayat
import halaman_cari_mahasiswa.urls as halaman_cari_mahasiswa
from django.views.generic import RedirectView
from django.core.urlresolvers import reverse_lazy

urlpatterns = [
    url(r'^admin/', admin.site.urls),
    url(r'fitur1/', include(halaman_login_status, namespace = 'fitur1')),
    url(r'fitur2/', include(halaman_profile, namespace = 'fitur2')),
    url(r'riwayat/', include(halaman_riwayat, namespace = 'riwayat')),
    url(r'cari-mahasiswa/', include(halaman_cari_mahasiswa, namespace = 'cari-mahasiswa')),
    url(r'^$', RedirectView.as_view(url='/fitur1/', permanent = True)),
]
